package connection

import (
	"database/sql"
	"log"
)

func Connect() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/learngo_postest_individu")

	if err != nil {
		log.Fatal(err)
	}

	return db
}
