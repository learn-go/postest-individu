module gitlab.com/learn-go/postest-individu

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
)
