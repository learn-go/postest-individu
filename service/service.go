package service

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/learn-go/postest-individu/connection"
)

type JSON map[string]interface{}

type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type UserCollection struct {
	Users []User `json:"users"`
}

func db() *sql.DB {
	return connection.Connect()
}

func GetAllUsers(c echo.Context) error {
	db := db()

	sql := "select id, name from user"
	rows, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer rows.Close()

	result := UserCollection{}
	for rows.Next() {
		user := User{}
		err2 := rows.Scan(&user.ID, &user.Name)
		if err2 != nil {
			return err2
		}
		result.Users = append(result.Users, user)
	}
	return c.JSON(http.StatusOK, result.Users)
}

func InsertNewUser(c echo.Context) error {
	db := db()

	var newUser User

	if err := c.Bind(&newUser); err != nil {
		return err
	}
	sql := "INSERT INTO user (name) VALUES(?)"
	stmt, err := db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err2 := stmt.Exec(newUser.Name)
	if err2 != nil {
		return err2
	}

	id, err := result.LastInsertId()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, JSON{
		"created": id,
	})
}

func UpdateUser(c echo.Context) error {
	db := db()

	var updateUser User

	id, _ := strconv.Atoi(c.Param("id"))
	if err := c.Bind(&updateUser); err != nil {
		return err
	}
	sql := "UPDATE user SET name = ? WHERE id = ?"
	stmt, _ := db.Prepare(sql)
	defer stmt.Close()

	_, err := stmt.Exec(updateUser.Name, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, JSON{
		"updated": id,
	})
}

func DeleteUser(c echo.Context) error {
	db := db()

	id, _ := strconv.Atoi(c.Param("id"))
	sql := "DELETE FROM user WHERE id = ?"
	stmt, _ := db.Prepare(sql)
	defer stmt.Close()

	_, err := stmt.Exec(id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, JSON{
		"deleted": id,
	})
}
