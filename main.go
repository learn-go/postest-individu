package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"gitlab.com/learn-go/postest-individu/service"
)

func main() {
	e := echo.New()

	e.GET("/", service.GetAllUsers)
	e.POST("/", service.InsertNewUser)
	e.PUT("/:id", service.UpdateUser)
	e.DELETE("/:id", service.DeleteUser)
	e.Logger.Fatal(e.Start(":8000"))
}
